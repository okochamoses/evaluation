<?php
// App root
define("APPROOT", dirname(__FILE__, 2));
// Url root
define("URLROOT", "http://localhost/evaluation");
// Define Sitename
define("SITENAME", "NYSC Evaluation");


// Define database parameters
define("DB_CONNECTION", "mysql");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "nysc");
define("DB_HOST", "127.0.0.1");
define("DB_OPTIONS", [
  PDO::ATTR_PERSISTENT => true,
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
]);
