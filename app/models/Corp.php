<?php

class Corp
{
    private $db;
    
    public function __construct()
    {
        $this->db = new Database;
    }

    public function login($stateCode, $identifier)
    {
        try {
            $this->db->query("SELECT * FROM corps 
                            WHERE stateCode = :stateCode AND 
                            (callupNumber = :identifier OR lastname = :identifier)");
            // Bind values
            $this->db->bind("stateCode", $stateCode);
            $this->db->bind("identifier", $identifier);
        
            return $this->db->getOne();
        } catch (PDOException $e) {
            // echo $e->getMessage();
            return false;
        }
    }
}
