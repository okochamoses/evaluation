<?php
require_once "config/config.php";
require 'vendor/autoload.php';

// Require Helpers
require_once "helpers/url_helper.php";
require_once "helpers/session_helper.php";

spl_autoload_register(function ($className) {
    require_once "libraries/" . $className . ".php";
});
