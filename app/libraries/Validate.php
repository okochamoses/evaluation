<?php

class Validate
{
    private $errors;

    public function __construct()
    {
        $this->errors = (object)[];
    }
    
    public function input($data, $inputName, $label="This", int $min=null, int $max=null)
    {
        if ($data == "") {
            $this->errors->$inputName = $label. " field cannot be empty";
        }
        if ($min != null && strlen($data) < $min) {
            $this->errors->$inputName = $label. " field must be greater than " . $min . " characters";
        }
        if ($max != null && strlen($data) > $max) {
            $this->errors->$inputName = $label. " field must be less than " . $max . " characters";
        }
    }


    public function email(String $email)
    {
        switch ($email) {
          case "":
            $this->errors->email = "Email field cannot be empty";
            break;

            case !strpbrk($email, "@"):
            $this->errors->email = "Please enter a valid email";
            break;

            default:
            break;
        }
    }

    public function password(String $password)
    {
        switch ($password) {
            case "":
            $this->errors->password = "Password field cannot be empty";
            break;

            case strlen($password) < 6:
            $this->errors->password = "Password must be at least 6 characters";
            break;

            default:
            break;
      }
    }

    public function confirmPassword(String $password, String $confirmPassword)
    {
        if ($password != $confirmPassword) {
            $this->errors->confirmPassword = "Passwords do not match";
        }
    }

    public function stateCode(String $stateCode)
    {
        $length = strlen($stateCode) == 11;
        $stateCodeArr = explode("/", $stateCode);
        if (count($stateCodeArr) == 3) {
            $firstElem = strlen($stateCodeArr[0]) == 2;
            $secondElem = strlen($stateCodeArr[1]) == 3;
            $lastElem = strlen($stateCodeArr[2]) == 4;
            $checkElem = $firstElem && $secondElem && $lastElem;
        }
        if (!($length && $checkElem)) {
            $this->errors->stateCode = "Please enter a valid state code value";
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }
}

// $validate = new Validate;
// $validate->validateEmail("moses@okocjaemi");
// $validate->validatePassword("fa4s");
// var_dump($validate->getErrors());
