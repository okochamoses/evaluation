<?php
  /*
   * Base Controller
   * Loads the model and views
   */
   
   class Controller
   {
       private $loader;
       private $twig;
       //Load model
       public function model($model)
       {
           // Require model file
           require_once "../app/models/" . $model . ".php";

           // Instantiate model
           return new $model();
       }

       //Load view
       public function view($view, $data = [])
       {
           //// Check for view file
           //  if(file_exists("../app/views/" . $view . ".php")) {
           //   require_once "../app/views/" . $view . ".php";
           //  }
           //  else {
           //    die("View does not exist");
           //  }
           //  require_once("../public/index.php");
         
           $this->loader= new Twig_Loader_Filesystem('../app/views');
           $this->twig = new Twig_Environment($this->loader);

           // Define config files
           $viewData = array(
            "URLROOT" => URLROOT,
            "SITENAME" => SITENAME,
            "data" => $data
          );
           echo $this->twig->render($view.".twig", $viewData);
       }
   }
