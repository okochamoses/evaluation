<?php

/*
* App core Class
* Creates URL & loads core controller
* URL FORMAT - /controller/method/params
*/

class Core
{
    protected $currentController = "Home";
    protected $currentMethod = "index";
    protected $params = [];

    public function __construct()
    {
        $url = $this->getUrl();
        
        // Check for controller with first element
        if (file_exists("../app/controllers/". ucwords($url[0]) .".php")) {
            // Set controller
            $this->currentController = ucwords($url[0]);
            unset($url[0]);
        }

        // Require current controller file
        require_once "../app/controllers/". $this->currentController .".php";

        // Instantiate controller class
        $this->currentController = new $this->currentController;

        // Check for 2nd parameter
        if (isset($url[1])) {
            // Check if method exiists in controller
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
            }
            unset($url[1]);
        }
        
        // Get params
        $this->params = $url ? array_values($url) : [];

        // Call a callback with array of params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }

    public function getUrl()
    {
        if (isset($_GET["url"])) {
            // Remove "/" from end of url
            $url = rtrim($_GET["url"], "/");

            // Remove all disallowed URL chars
            $url = filter_var($url, FILTER_SANITIZE_URL);

            // Convert URL params to array
            $url = explode("/", $url);
            return $url;
        }
    }

    public function getParams()
    {
    }
}
