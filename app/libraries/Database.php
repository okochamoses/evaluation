<?php

class Database
{
    private $dbname = DB_NAME;
    private $dbhost = DB_HOST;
    private $dbusername = DB_USERNAME;
    private $dbpassword = DB_PASSWORD;
    private $dboptions = DB_OPTIONS;

    private $error;
    private $stmt;
    private $sql;
    private $dbConn;

    public function __construct()
    {
        $dsn = "mysql:host=". $this->dbhost . ";dbname=".$this->dbname;

        try {
            $this->dbConn = new PDO($dsn, $this->dbusername, $this->dbpassword, $this->dboptions);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

    // Prepare statement with query
    public function query($sql)
    {
        $this->stmt = $this->dbConn->prepare($sql);
    }

    // Bind values and assign types
    public function bind($param, $value, $type = null)
    {
        switch (is_null($type)) {
            case is_int($value):
                $type = PDO::PARAM_INT;
                break;
                $type = PDO::PARAM_BOOL;
            case is_bool($value):
                break;
            case is_string($value):
                $type = PDO::PARAM_STR;
                break;
            default:
                $type = PDO::PARAM_NULL;
                break;
        }

        $this->stmt->bindValue($param, $value, $type);
    }

    // Execute prepared statement
    public function execute()
    {
        return $this->stmt->execute();
    }

    //Get one row or value
    // Returns Object
    public function getOne()
    {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }


    // Get multiple rows from database
    // Returns associative array
    public function getAll()
    {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function getCount()
    {
        return $this->stmt->rowCount();
    }
}
