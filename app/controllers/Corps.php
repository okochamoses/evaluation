<?php

class Corps extends Controller
{
    private $errors;
    private $validate;
    private $input;

    public function __construct()
    {
        $this->input = (object)[];
        $this->corp = $this->model("Corp");
        $this->validate = new Validate;
    }

    public function index()
    {
        $title = "Corp Member Login";
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            // Validate User Input
            $this->validate->stateCode($_POST["stateCode"]);
            $this->validate->input($_POST["identifier"], "identifier", "Call Up Number / Surname", 2, 12);

            // Get validation errors
            $errors = $this->validate->getErrors();
                        
            // Persist already inputed data
            $this->input->stateCode = $_POST["stateCode"];
            $this->input->identifier = $_POST["identifier"];

            // Set data to be sent to page
            $data = ["title" => $title, "errors" => $errors, "input" => $this->input];

            if (count($errors)) {
                $user = $this->corp->login($this->input->stateCode, $this->input->identifier);
                if (!$user) {
                    // var_dump("MOSES IT WORKS NOT");
                    newFlash("failed", "Login failed, please check credentials and try again", "alert alert-danger");
                } else {
                    $this->createUserSession($user);
                    // var_dump("IT WORKS");
                    redirect("corps/dashboard");
                }
            }

            // Return view along with data
            $this->view("corps/index", $data);
        } else {
            $data = ["title" => $title];
            // Return view along with data
            $this->view("corps/index", $data);
        }
    }

    public function dashboard()
    {
        $data = ["title" => "Dashboard"];
        if ($this->authenticated()) {
            $this->view("corps/dashboard", $data);
        } else {
            echo "USER NOT AUTHENTICATED - ";
            redirect("corps/index");
        }
    }

    private function authenticated()
    {
        if (!isset($_SESSION["corp_id"])) {
            return false;
        }
        return true;
    }

    private function createUserSession($user)
    {
        $_SESSION["corp_id"] = $user->id;
        $_SESSION["firstname"] = $user->firstname;
        $_SESSION["lastname"] = $user->lastname;
        $_SESSION["stateCode"] = $user->stateCode;
        $_SESSION["callupNumber"] = $user->callupNumber;
    }

    public function logout()
    {
        session_destroy();
        redirect("corps/index");
    }
}
