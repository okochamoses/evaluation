<?php

class Home extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $data = [
            "title" => "Home"
        ];
        $this->view('index', $data);
    }

    public function about($id)
    {
        echo $id;
    }
}
