# NYSC EVALUATIONS

Here is a little bit of documentation for this mini framework. The framework tries it's best to adhere to the MVC pattern

# Basic Overview

- The applications entry point is `public/index.php`
- Most of the prettyURL's are done with `.htaccess` files
- The routing logic is carried out by `libraries/Core.php`
- Models are stored in `app/models`, Views in `app/views` and controllers in `app/controllers`
- The apps configuration can be changed via the `app/config/congig.php` files
- The `app/helpers` store helper functions
- `app/init.php` botstraps our files needed to run the app
- All the library files are autoloded into `app/init.php`
- The app uses the twig template engine to render views

---

# Installation

Pull in this repository
Navigate to `/app` directory in your terminal and run `composer install`
An `SQL.sql` file is bundled with the repository, use it to create a compatible database.
Set variables in `app/config/config.js`

```
// Only Change these variables
define("DB_USERNAME", "root"); //Choose username
define("DB_PASSWORD", ""); // CHoose password
define("DB_NAME", "nysc"); // Choose DB name
```

## Start localserver

# Work

### Models

##### Creating a Model

- To create a model, you need to create a new file in the app/models directory and the filename must be stored with caps-first and singular for i.e ("Animal" !"animal")

- The class name defined must be the same name as the filename.

- To make use of the controller, do the following

```
    private $db;
    public function __construct()
        {
            $this->db = new Database;
        }
```

##### Handling queries in models

To hanle queries to the Database, a set of functions can be called from `app/libraries/Database.php` file. Please view the file to see them, it's very self explanatory if you have experience with PDO

---

### Controllers

-The controllers will get data from the models and render them to the views.

The controllers are also sort of responsible for routing. The names of the functions correspond to the url

- e.g visiting `URLROOT/corps/index` will use the `index()` function in the Corps.php controller.
- e.g visiting `URLROOT/corps/form2a/99` will use the form2a($param) function in the Corps.php controller, where `$param = 99`

Views can be loaded by this function `$this->view("corps/index", $data)` where `$data` is any data that yu want to pass to the view. The view in this paricaular instance is loaded from `app/views/corps/index.php` file.

Models can be accessed via `$this->modelName = $this->model("ModelName");` i.e `$this->corp = new Corp;`

---

### Views

_UI people, this is your domain_
Views are loaded in controllers. They can get data from the controller via a $data variable passed from the controllers
The views are controlled by twig template engine and as such should use twig syntax and .twig extentions
You can create include files to separate your code a bit and to avoid repitition for things like navbar
-Linked files can be put in `public/**` in their respective file types.

---

#### Validate

This probably needs some work, but for now, just intantiate a new `Validate` class and pull in functions to validate user input. The file is located at `app/libraries/Validate.php`. Don't worry, it's autoloaded already.

---

### Thoughts

- Why I think this will be good. We can separate ourselves into different groups very easily with this model and there won't be any conflict.

- Seems maintainable and easy to grasp.
